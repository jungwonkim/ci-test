#include <mpi.h>
#include <stdio.h>

int rank;

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    printf("Hello World from rank[%d]!!\n", rank);
    MPI_Finalize();
    return 0;
}
